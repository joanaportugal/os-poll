import React, { useState } from "react";
import { PieChart } from "react-minimal-pie-chart";
import { connect } from "react-redux";

import { addVoteAction } from "./store/actions";

function App({ votes, addVote }) {
  const [voted, setVoted] = useState(false);

  const handleVote = (e) => {
    setVoted(true);
    addVote(e.target.value);
  };

  return (
    <>
      <h1>OS Poll</h1>
      {!voted ? (
        <section id="voting">
          <p>Which OS system do you prefer?</p>
          <div>
            <button value="windows" onClick={(e) => handleVote(e)}>
              Windows
            </button>
            <button value="macos" onClick={(e) => handleVote(e)}>
              MacOs
            </button>
            <button value="linux" onClick={(e) => handleVote(e)}>
              Linux
            </button>
          </div>
        </section>
      ) : (
        <section>
          <PieChart
            data={[
              { title: "Windows", value: votes.votesWindows, color: "blue" },
              { title: "MacOs", value: votes.votesMacOS, color: "#ffc" },
              { title: "Linux", value: votes.votesLinux, color: "#333" },
            ]}
          />
          <div>
            <p>Legend:</p>
            <p>Blue - Windows</p>
            <p>White - MacOs</p>
            <p>Grey - Linux</p>
          </div>
        </section>
      )}
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    votes: state,
  };
};

const mapDispatchToProps = (dispatch) => ({
  addVote: (osOption) => dispatch(addVoteAction(osOption)),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
