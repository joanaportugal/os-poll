const initialState = {
  votesWindows: 0,
  votesMacOS: 0,
  votesLinux: 0,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_VOTE":
      return {
        ...state,
        votesWindows:
          action.osOption === "windows"
            ? state.votesWindows + 1
            : state.votesWindows,
        votesMacOS:
          action.osOption === "macos" ? state.votesMacOS + 1 : state.votesMacOS,
        votesLinux:
          action.osOption === "linux" ? state.votesLinux + 1 : state.votesLinux,
      };
    default:
      return state;
  }
};

export default reducer;
